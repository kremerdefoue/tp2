#include <gtest/gtest.h>
#include <math.h>

#include "generator.h"
#include "hasher.h"

/*
 * Test des fonctions de hachage
 */

static const char* k_fname = "test_hashing_data.bin";
static const size_t sz = (1 << 10); // 1 ko

// Somme de contrôle que l'on est supposé obtenir pour le fichier de test
// généré automatiquement.
static const unsigned char hash_ok[] = {             //
    0xfd, 0xf4, 0x43, 0x6f, 0x01, 0xc5, 0x05, 0x9e,  //
    0xf0, 0x6a, 0xde, 0xe2, 0x6a, 0x5d, 0xa8, 0x32,  //
    0x0e, 0xbc, 0xbd, 0x8b, 0xdf, 0xa4, 0x21, 0xd9,  //
    0x36, 0xcb, 0x26, 0xe3, 0xbb, 0xc6, 0x2a, 0x10};

TEST(ComputeHash, SinglePart) {
  generate_file(k_fname, sz, 1);
  struct hash_info* hi = make_hash_info();
  ASSERT_EQ(compute_hash_basic(k_fname, 0, hi), 0);
  EXPECT_EQ(memcmp(hi->hash, hash_ok, hi->size), 0);
  free_hash_info(hi);
}

TEST(ComputeHash, MultiPartSysRead) {
  generate_file(k_fname, sz, 1);
  struct hash_info* hi = make_hash_info();
  ASSERT_EQ(compute_hash_sysread(k_fname, 10, hi), 0);
  EXPECT_EQ(memcmp(hi->hash, hash_ok, hi->size), 0);
  free_hash_info(hi);
}

TEST(ComputeHash, MultiPartFread) {
  generate_file(k_fname, sz, 1);
  struct hash_info* hi = make_hash_info();
  ASSERT_EQ(compute_hash_fread(k_fname, 10, hi), 0);
  EXPECT_EQ(memcmp(hi->hash, hash_ok, hi->size), 0);
  free_hash_info(hi);
}

TEST(ComputeHash, MultiPartMMap) {
  generate_file(k_fname, sz, 1);
  struct hash_info* hi = make_hash_info();
  ASSERT_EQ(compute_hash_mmap(k_fname, 10, hi), 0);
  EXPECT_EQ(memcmp(hi->hash, hash_ok, hi->size), 0);
  free_hash_info(hi);
}
