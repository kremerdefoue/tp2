#include <fcntl.h>
#include <sodium.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

// Ne pas changer le seed, sinon les tests vont échouer. Les tests prennent
// pour acquis que CE seed est utilisé.
static const unsigned char seed[randombytes_SEEDBYTES] = {
    0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE,  //
    0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE,  //
    0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE,  //
    0xCA, 0xFE, 0xBA, 0xBE, 0xCA, 0xFE, 0xBA, 0xBE,  //
};

int generate_file(const char* path, size_t size, int overwrite) {
  // Par précaution, on ne remplace pas un fichier sans l'option
  if (!overwrite) {
    if (access(path, F_OK) == 0) {
      printf("Error: file exists and overwrite disabled\n");
      printf("Use the -f option to overwrite the file\n");
      return -1;
    }
  }

  int fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  if (fd < 0) {
    perror("open");
    return -1;
  }

  if (ftruncate(fd, size) < 0) {
    perror("ftruncate");
    return -1;
  }

  void* buf = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (buf == MAP_FAILED) {
    perror("mmap");
    return -1;
  }

  randombytes_buf_deterministic(buf, size, seed);

  munmap(buf, size);
  return 0;
}
