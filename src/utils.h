#ifndef TP2_UTILS_H_
#define TP2_UTILS_H_

#include <sys/time.h>
#include <time.h>

// timeval
double timeval_delta(struct timeval* a, struct timeval* b);
double timeval_to_second(struct timeval* ts);

// timespec
double timespec_delta(struct timespec* a, struct timespec* b);
double timespec_to_second(struct timespec* ts);

#endif
