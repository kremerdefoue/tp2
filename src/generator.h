#ifndef GENERATOR_H
#define GENERATOR_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

int generate_file(const char* path, size_t size, int overwrite);

#ifdef __cplusplus
}
#endif

#endif
