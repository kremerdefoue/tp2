#include "utils.h"

/*
 * Utilitaires pour lecture et conversion du temps
 */
double timeval_to_second(struct timeval* ts) {
  double sec = ts->tv_sec;
  double usec = ts->tv_usec;
  return sec + usec / 1E6;
}

double timeval_delta(struct timeval* a, struct timeval* b) {
  return timeval_to_second(a) - timeval_to_second(b);
}

double timespec_to_second(struct timespec* ts) {
  double sec = ts->tv_sec;
  double nsec = ts->tv_nsec;
  return sec + nsec / 1E9;
}

double timespec_delta(struct timespec* a, struct timespec* b) {
  return timespec_to_second(a) - timespec_to_second(b);
}
