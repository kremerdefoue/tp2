#ifndef HASHER_H_
#define HASHER_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

struct hash_info {
  unsigned char* hash;
  unsigned long size;
  int is_valid;
};

// Hasher API

typedef int (*hasher_t)(  //
    const char* fname,    //
    int chuck_size,       //
    struct hash_info* hi  //
);

struct hash_info* make_hash_info();
void free_hash_info(struct hash_info* info);
void init_hash(struct hash_info* info);
void print_hash(FILE* out, struct hash_info* hi, int format);

// Hasher functions
int compute_hash(const char* fname, struct hash_info* hi);

// Functions for benchmarks
int compute_hash_basic(const char* fname, int chunk_size, struct hash_info* hi);
int compute_hash_sysread(const char* fname, int chunk_size, struct hash_info* hi);
int compute_hash_fread(const char* fname, int chunk_size, struct hash_info* hi);
int compute_hash_mmap(const char* fname, int chunk_size, struct hash_info* hi);

#ifdef __cplusplus
}
#endif

#endif
