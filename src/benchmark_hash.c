#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "generator.h"
#include "hasher.h"
#include "utils.h"

struct item {
  const char* name;
  hasher_t func;
};

// Tableau des fonctions pour la mesure de performance
static const struct item items[] = {
    {"basic", compute_hash_basic},
    {"sysread", compute_hash_sysread},
    {"fread", compute_hash_fread},
    {"mmap", compute_hash_mmap},
};
// On calcule à la compilation le nombre d'élément dans le tableau en divisant
// la taille totale par la taille d'un élément.
static const int num_items = sizeof(items) / sizeof(items[0]);

void write_benchmark_results(const char* fname, double* df, int rows) {
  // imprimer resultats
  FILE* dat = fopen(fname, "w");

  // entete
  fprintf(dat, "%-8s", "chunk_sz");
  for (int i = 0; i < num_items; i++) {
    const struct item* curr = &items[i];
    fprintf(dat, " %-14s", curr->name);
  }
  fprintf(dat, "\n");

  // contenu
  for (int j = 0; j < rows; j++) {
    fprintf(dat, "%-8ld", (1UL << j));
    for (int i = 0; i < num_items; i++) {
      double val = df[i + j * num_items];
      fprintf(dat, " %0.8E", val);
    }
    fprintf(dat, "\n");
  }
}

int main(int argc, char** argv) {
  int pow_max = 16;

  // argv[1] : fichier à traiter
  if (argc < 2) {
    printf("missing argument: file\n");
    return 1;
  }
  const char* f_name = argv[1];

  if (access(f_name, R_OK) < 0) {
    perror("access");
    return 1;
  }

  size_t data_size = num_items * pow_max * sizeof(double);
  double* data_elapsed = malloc(data_size);
  struct hash_info* hi = make_hash_info();

  /*
   * Banc d'essai de performance
   */

  for (int col = 0; col < num_items; col++) {
    const struct item* curr = &items[col];

    for (int row = 0; row < pow_max; row++) {
      unsigned long chunk_size = 1UL << row;

      /*
       * Faire un appel de fonction en dehors de la mesure pour
       * obtenir un résultat plus stable
       */
      curr->func(f_name, chunk_size, hi);

      struct timespec t1, t2;
      clock_gettime(CLOCK_MONOTONIC, &t1);
      int repeat = 0;
      double min_elapsed = 0.1;
      double elapsed = 0;
      while (elapsed < min_elapsed) {
        init_hash(hi);
        repeat++;
        curr->func(f_name, chunk_size, hi);
        clock_gettime(CLOCK_MONOTONIC, &t2);
        elapsed = timespec_delta(&t2, &t1);
      }

      // Calculer la moyenne du temps pour une exécution
      double etime = timespec_delta(&t2, &t1) / repeat;
      double etime_ms = etime * 1000;

      fprintf(stdout, "BM %8s %10ld %3d %10.2f ms\n", curr->name, chunk_size, repeat, etime_ms);

      data_elapsed[col + row * num_items] = etime;
    }
  }

  write_benchmark_results("result_elapsed.dat", data_elapsed, pow_max);

  free_hash_info(hi);
  free(data_elapsed);

  printf("done!\n");
  return 0;
}
