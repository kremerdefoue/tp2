#include "hasher.h"

#include <fcntl.h>
#include <sodium.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


struct hash_info* make_hash_info() {
  struct hash_info* info = malloc(sizeof(struct hash_info));
  info->hash = malloc(crypto_generichash_BYTES);
  info->size = crypto_generichash_BYTES;
  info->is_valid = 0;
  init_hash(info);
  return info;
}

void free_hash_info(struct hash_info* info) {
  free(info->hash);
  free(info);
}

void init_hash(struct hash_info* info) {
  info->is_valid = 0;
  memset(info->hash, 0, info->size);
}

// Calcule la somme de contrôle du fichier passé en paramètre.

void print_hash(FILE* out, struct hash_info* hi, int format) {
  for (unsigned long i = 0; i < hi->size; i++) {
    if (format) {
      fprintf(out, "0x");
    }
    fprintf(out, "%02x", (0xFF & hi->hash[i]));
    if (format && i < (hi->size - 1)) {
      fprintf(out, ", ");
    }
    if (format && (((i + 1) % 8) == 0)) {
      fprintf(out, "\n");
    }
  }
  fprintf(out, "\n");
}

// TODO: Bonne version à utiliser
int compute_hash(const char* fname, struct hash_info* hi) {
  return compute_hash_basic(fname, 0, hi);
}

int compute_hash_basic(const char* fname, int chunk_size, struct hash_info* hi) {
  (void)chunk_size;
  char* buf = NULL;
  int fd;
  int ret = 0;
  struct stat info;

  init_hash(hi);

  // Ouvrir le fichier pour lequel on veut calculer la somme de contrôle.
  if ((fd = open(fname, O_RDONLY)) < 0) {
    perror("open");
    return -1;
  }

  // Obtenir la taille du fichier pour savoir combien de mémoire il faut
  // allouer.
  if ((ret = stat(fname, &info)) < 0) {
    perror("stat");
    goto out;
  }

  /*
   * Version simple: "Generic hashing Single-part example without a key"
   * La variable chunk_size n'est pas utilisée ici.
   *
   * On alloue de la mémoire pour contenir tout le fichier Note: Ça marche...
   * mais impossible de traiter des gros fichiers, car on manque de mémoire.
   * Comment faire??? SVP j'ai besoin d'aide d'un(e) expert(e)!!!
   */
  buf = malloc(info.st_size);
  if (!buf) {
    printf("malloc error\n");
    ret = -1;
    goto out;
  }
  ret = read(fd, buf, info.st_size);
  if (ret != info.st_size) {
    perror("read");
    goto out;
  }

  ret = crypto_generichash(hi->hash,      // La somme de contrôle (sortie)
                           hi->size,      // Nombre d'octets de la somme de contrôle
                           buf,           // Données à vérifier (entrée)
                           info.st_size,  // Taille des données à vérifier
                           NULL,          // Clé de sécurité (aucune)
                           0              // Taille de la clé (non-applicable)
  );

  // Un code de retour de zéro indique un succès
  if (ret == 0) {
    hi->is_valid = 1;
  }

  // On utilise un goto pour s'assurer qu'à chaque fois qu'il y a une erreur
  // après le open(), on s'assure de fermer le fichier sans dupliquer l'appel à
  // close()
out:
  close(fd);
  free(buf);
  return ret;
}

int compute_hash_sysread(const char* fname, int chunk_size, struct hash_info* hi) {
  // TODO: utiliser read()
  (void)chunk_size;
  int fd;
  int ret = 0;
  struct stat info;
  char buffer[chunk_size];


  init_hash(hi);

  // Ouvrir le fichier pour lequel on veut calculer la somme de contrôle.
  if ((fd = open(fname, O_RDONLY)) < 0) {
    perror("open");
    return -1;
  }

  // Obtenir la taille du fichier pour savoir combien de mémoire il faut
  // allouer.
  if ((ret = stat(fname, &info)) < 0) {
    perror("stat");
    goto out;
  }

  crypto_generichash_state state;
  if (crypto_generichash_init(&state, NULL, 0, hi->size) < 0) {
    perror("crypto_generichash_init");
      return -1;
  }

  ssize_t bytes_read;
  while ((bytes_read = read(fd, buffer, chunk_size)) > 0) {
    if (crypto_generichash_update(&state, buffer, bytes_read) < 0) {
      perror("crypto_generichash_update");
      return -1;
    }
  }

  if (bytes_read < 0) {
    perror("read");
      return -1;
  }

  if (crypto_generichash_final(&state, hi->hash, hi->size) < 0) {
    perror("crypto_generichash_final");
    return -1;
  }
  if (ret == 0) {
    hi->is_valid = 1;
  }
  out:
    close(fd);
    //free(buffer);
    return ret;
}


int compute_hash_fread(const char* fname, int chunk_size, struct hash_info* hi) {
  FILE *fp;
  char buffer[chunk_size];
  int ret = 0;

  init_hash(hi);

  // Ouvrir le fichier pour lequel on veut calculer la somme de contrôle.
  if ((fp = fopen(fname, "rb")) == NULL) {
    perror("fopen");
    return -1;
  }

  crypto_generichash_state state;
  if (crypto_generichash_init(&state, NULL, 0, hi->size) < 0) {
    perror("crypto_generichash_init");
    ret = -1;
    goto out;
  }

  size_t bytes_read;
  while ((bytes_read = fread(buffer, 1, chunk_size, fp)) > 0) {
    if (crypto_generichash_update(&state, buffer, bytes_read) < 0) {
      perror("crypto_generichash_update");
      ret = -1;
      goto out;
    }
  }

  if (bytes_read == 0 && ferror(fp)) {
    perror("fread");
    ret = -1;
    goto out;
  }

  if (crypto_generichash_final(&state, hi->hash, hi->size) < 0) {
    perror("crypto_generichash_final");
    ret = -1;
    goto out;
  }

  hi->is_valid = 1;

out:
  fclose(fp);
  return ret;
}


int compute_hash_mmap(const char* fname, int chunk_size, struct hash_info* hi) {
    int fd;
    int ret = 0;
    struct stat info;
    char* file_content = NULL;

    init_hash(hi);

    // Ouvrir le fichier pour lequel on veut calculer la somme de contrôle.
    if ((fd = open(fname, O_RDONLY)) < 0) {
        perror("open");
        return -1;
    }

    // Obtenir la taille du fichier pour savoir combien de mémoire il faut
    // allouer.
    if ((ret = fstat(fd, &info)) < 0) {
        perror("fstat");
        goto out;
    }

    // Mmap le contenu du fichier en mémoire
    file_content = mmap(NULL, info.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (file_content == MAP_FAILED) {
        perror("mmap");
        goto out;
    }

    crypto_generichash_state state;
    if (crypto_generichash_init(&state, NULL, 0, hi->size) < 0) {
        perror("crypto_generichash_init");
        ret = -1;
        goto out;
    }

    // Calculer le hash
    ret = crypto_generichash_update(&state, file_content, info.st_size);
    if (ret < 0) {
        perror("crypto_generichash_update");
        goto out;
    }

    ret = crypto_generichash_final(&state, hi->hash, hi->size);
    if (ret < 0) {
        perror("crypto_generichash_final");
        goto out;
    }

    if (ret == 0) {
        hi->is_valid = 1;
    }

out:
    if (file_content) {
        munmap(file_content, info.st_size);
    }
    close(fd);
    return ret;
}
