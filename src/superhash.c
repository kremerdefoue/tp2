#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hasher.h"

int main(int argc, char** argv) {
  int ret = 0;
  if (sodium_init() == -1) {
    return 1;
  }

  if (argc < 2) {
    printf("missing argument\n");
    return 1;
  }
  int format = 0;
  const char* fname = argv[1];

  if (argc >= 3) {
    if (strcmp(argv[2], "--format=byte") == 0) {
      format = 1;
    }
  }

  struct hash_info* hi = make_hash_info();
  if ((ret = compute_hash_mmap(fname,10, hi)) < 0) {
    goto out;
  }

  print_hash(stdout, hi, format);

out:
  free_hash_info(hi);
  return ret;
}
