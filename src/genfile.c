#include <sodium.h>
#include <stdlib.h>
#include <string.h>

#include "generator.h"

int main(int argc, char** argv) {
  if (sodium_init() == -1) {
    return 1;
  }
  if (argc < 3) {
    printf("missing arguments\n");
    return 1;
  }

  const char* fname = argv[1];
  size_t size = atoi(argv[2]);
  int overwrite = 0;
  if (argc >= 4) {
    if ((strcmp(argv[3], "-f") == 0) ||  //
        (strcmp(argv[3], "--force") == 0)) {
      overwrite = 1;
    }
  }

  printf("fname %s\n", fname);
  printf("size %ld\n", size);

  if (generate_file(fname, size, overwrite) < 0) {
    printf("Error in generate_file\n");
    return 1;
  }

  return 0;
}
